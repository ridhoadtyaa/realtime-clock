import ClockClass from './components/ClockClass';
import ClockFunc from './components/ClockFunc';

function App() {
	return (
		<div style={{ width: '100%', textAlign: 'center', maxWidth: 500, margin: '0 auto' }}>
			<h1>Realtime Clock</h1>
			<hr />
			<ClockClass />
			{/* <ClockFunc /> */}
		</div>
	);
}

export default App;
