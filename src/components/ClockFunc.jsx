import { useEffect, useState } from 'react';

const ClockFunc = () => {
	const [date, setDate] = useState(new Date());

	const tick = () => setDate(new Date());

	useEffect(() => {
		const timer = setInterval(() => tick(), 1000);

		return () => {
			clearInterval(timer);
		};
	}, []);
	return (
		<>
			<h1>{date.toLocaleTimeString()}</h1>
		</>
	);
};

export default ClockFunc;
